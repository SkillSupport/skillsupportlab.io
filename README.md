## Welcome to SkillSupport Labs

스킬서포트 실습 자료실에 오신 것을 환영합니다.

이 사이트는 [(주)스킬서포트](http://www.skillsupport.co.kr) 에서 사용되는 교육의 실습 목적 이외에 자료의 무단 수정 및 유출을 금지합니다.

해당 실습 파일은 일부는 스킬서포트 실습환경에서만, 일부는 특정 일자에만 열람할수 있도록 설정되어 있습니다.

<br>

## 해당되는 강의명을 클릭합니다.


[Skill Support 과정 : Azure Infra Fundamental Training](http://gitlab.skillsupport.co.kr/Azure/ss-iaas-basic)

[Skill Support 과정 : Azure Infra Advanced Training](http://gitlab.skillsupport.co.kr/Azure/ss-iaas-advanced/)

[Microsoft 공인 과정 : AZ-104 (Azure 관리자)](http://gitlab.skillsupport.co.kr/Azure/az-104)

[Microsoft 공인 과정 : AZ-204 (Azure 개발자)](http://gitlab.skillsupport.co.kr/Azure/az-204)

[Microsoft 공인 과정 : AZ-303 (Azure 아키텍쳐 기술)](http://gitlab.skillsupport.co.kr/Azure/az-303)

[Microsoft 공인 과정 : AZ-900 (Azure 기초)](http://gitlab.skillsupport.co.kr/Azure/az-900)


<br>

`Copyright ⓒ (주)스킬서포트 All Rights Reserved.`
